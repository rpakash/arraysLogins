let data = [
  {
    id: 1,
    first_name: "Valera",
    last_name: "Pinsent",
    email: "vpinsent0@google.co.jp",
    gender: "Male",
    ip_address: "253.171.63.171",
  },
  {
    id: 2,
    first_name: "Kenneth",
    last_name: "Hinemoor",
    email: "khinemoor1@yellowbook.com",
    gender: "Polygender",
    ip_address: "50.231.58.150",
  },
  {
    id: 3,
    first_name: "Roman",
    last_name: "Sedcole",
    email: "rsedcole2@addtoany.com",
    gender: "Genderqueer",
    ip_address: "236.52.184.83",
  },
  {
    id: 4,
    first_name: "Lind",
    last_name: "Ladyman",
    email: "lladyman3@wordpress.org",
    gender: "Male",
    ip_address: "118.12.213.144",
  },
  {
    id: 5,
    first_name: "Jocelyne",
    last_name: "Casse",
    email: "jcasse4@ehow.com",
    gender: "Agender",
    ip_address: "176.202.254.113",
  },
  {
    id: 6,
    first_name: "Stafford",
    last_name: "Dandy",
    email: "sdandy5@exblog.jp",
    gender: "Female",
    ip_address: "111.139.161.143",
  },
  {
    id: 7,
    first_name: "Jeramey",
    last_name: "Sweetsur",
    email: "jsweetsur6@youtube.com",
    gender: "Genderqueer",
    ip_address: "196.247.246.106",
  },
  {
    id: 8,
    first_name: "Anna-diane",
    last_name: "Wingar",
    email: "awingar7@auda.org.au",
    gender: "Agender",
    ip_address: "148.229.65.98",
  },
  {
    id: 9,
    first_name: "Cherianne",
    last_name: "Rantoul",
    email: "crantoul8@craigslist.org",
    gender: "Genderfluid",
    ip_address: "141.40.134.234",
  },
  {
    id: 10,
    first_name: "Nico",
    last_name: "Dunstall",
    email: "ndunstall9@technorati.com",
    gender: "Female",
    ip_address: "37.12.213.144",
  },
];

function findPeopleWithGender(people, gender) {
  return people.filter((person) => {
    return person.gender === gender;
  });
}

function splitIpAddressIntoComponents(people) {
  return people.reduce((accu, person) => {
    accu.push(person.ip_address.split("."));

    return accu;
  }, []);
}

function sumOfComponentsOfIpAddresses(people, component) {
  return people.reduce((accu, person) => {
    return (accu += parseInt(person.ip_address.split(".")[component - 1]));
  }, 0);
}

function fullName(people) {
  return people.map(({ ...person }) => {
    person.full_name = person.first_name + " " + person.last_name;
    return person;
  });
}

function filterOutEmails(people, email) {
  return people.filter((person) => {
    if (person.email.split(".").at(-1) === email) {
      return person;
    }
  });
}

function calculateEmails(people, emails) {
  return people.reduce((accu, person) => {
    let extension = person.email.split(".").at(-1);

    if (emails.includes(extension)) {
      accu[extension] !== undefined
        ? (accu[extension] += 1)
        : (accu[extension] = 1);
    }

    return accu;
  }, {});
}

function sortFirstName(people) {
  return people.sort((personA, personB) => {
    return personB.first_name.localeCompare(personA.first_name);
  });
}
